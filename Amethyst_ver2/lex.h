
#ifndef LEX_H_
	#define LEX_H_

	void skip_comments();
	void lex_idnkey();
	void lex_array();
	void lex_int(int intNeg);
	void lex_str();
	void lex_op();
	void lex_punc();
    void lex_char();
    void out_lex();

	int lexcheck(char* strFirst, char* strSec);


    void skip_comments(){

        if( cur->ch == '#' ) {

            STRING token = (STRING)malloc(sizeof(STRING));
            int intTokCount = 0;

            if(cur->next->ch == '#'){

                while(!specialValue(cur->ch)){
                    *(token+intTokCount) = cur->ch;
                    cur = cur->next;
                    intTokCount++;
                }
                *(token+intTokCount) = '\0';
                add_tok(COM,token);

            }//---------------end of single line comment

            else if(  cur->next->ch == '*' ) {

                while( cur->ch != '*'|| cur->next->ch != '#' ){
                    *(token+intTokCount) = cur->ch;
                    intTokCount++;
                    cur = cur->next;
                }
                int i;
                for(i = 0; i<2; i++){
                    *(token+intTokCount) = cur->ch;
                    intTokCount++;
                    cur = cur->next;
                }
                *(token+intTokCount) = '\0';
                add_tok(COM,token);

	     }//---------------end of multi-line comment

	     else{

            while(!specialValue(cur->ch)){
                    *(token+intTokCount) = cur->ch;
                    intTokCount++;
                    cur = cur->next;
            }
            *(token+intTokCount) = '\0';
            add_tok(ID,token);

	     }//--------------end of unrec #

            free(token);

        }//-----end of first if

	}


	/*lex_array()
        identifies array token
	*/
	void lex_array(){

         if(cur->ch=='['&&isdigit(cur->next->ch)){

                STRING token = (STRING)malloc(sizeof(STRING));
                int intTokCount = 0;

                *(token+intTokCount) = cur->ch;
                cur = cur->next;
                intTokCount++;
                while(isdigit(cur->ch)){
                    *(token+intTokCount) = cur->ch;
                    intTokCount++;
                    cur = cur->next;
                }
                *(token+intTokCount)=cur->ch;
                intTokCount++;

                if(cur->ch==']'){
                        *(token+strlen(token))='\0';
                    add_tok(ARR,token);
                }else{
                    *(token+strlen(token))='\0';
                    add_tok(ERUV,token);
                }
                free(token);
                cur = cur->next;
        }
	}


	/* lex_idnkey()
	 * lexes identifiers and keywords
	 */
	void lex_idnkey(){

        if(isalpha(cur->ch)){

            STRING token = (STRING)malloc(sizeof(STRING));
            int intTokCount = 0;
            while(isalnum(cur->ch)&&!specialValue(cur->ch)&&!pancuators(cur->ch)){
                *(token+intTokCount) = cur->ch;
                intTokCount++;
                cur = cur->next;
            }
             *(token+intTokCount) = '\0';

             if(keywords(token)){
                    add_tok(KEY,token);
             }
             else if(noisewords(token)){
                    add_tok(NW,token);
             }
             else if(datatypes(token)){
                add_tok(DT,token);
            }
            else{
                add_tok(ID,token);
            }
            free(token);
        }
	}

	/* lex_int()
	 * lexes integers
	 */
	void lex_int(int intNeg){

	    if(isdigit(cur->ch)||intNeg==1){

            STRING token = (STRING) malloc(sizeof(STRING));
            int intTokCount = 0;

            if(cur->ch=='-'){
                *(token + intTokCount) = cur->ch;
                intTokCount++;
                cur = cur->next;
            }

            for(; isdigit(cur->ch); cur = cur->next) {
                  *(token + intTokCount) = cur->ch;
                  intTokCount++;
            }

             if(cur->ch=='.'){
                  *(token+intTokCount)='.';
                    intTokCount++;
                    cur = cur->next;

                    for(; isdigit(cur->ch); cur = cur->next) {
                        *(token + intTokCount) = cur->ch;
                        intTokCount++;
                    }

                    if(cur->ch=='.'){
                        //printf("\n------------------\n");
                        //while(cur->ch != ' '||cur->ch != '\n'||cur->ch!='\t'){
                        while(isdigit(cur->ch)||cur->ch!=' '){
                            //printf("%c",cur->ch);
                            *(token + intTokCount) = cur->ch;
                            intTokCount++;
                            cur = cur->next;
                        }
                        //printf("\n------------------\n");
                        token[strlen(token)] = '\0';
                        add_tok(ERUV,token);
                        free(token);
                        return;
                    }
                    token[strlen(token)] = '\0';
                    add_tok(FLT,token);
                    free(token);
                    return;
             }
             token[strlen(token)] = '\0';
            add_tok(INT,token);
            free(token);
	     }

	}

	/* lex_str()
	 * lexes string literals
	 */
	void lex_str(){

            if( cur->ch == '"' ) {

                STRING token = (STRING)malloc(sizeof(STRING));
                int intTokCount = 0;
                cur = cur->next;
                while(cur->ch != '"') {
                    *(token + intTokCount) = cur->ch;
                     intTokCount++;
                    cur = cur->next;
                }
                cur = cur->next;
                //*(token + intTokCount) = '"';
                token[intTokCount] = '\0';

                add_tok(STR,token);
                free(token);
	     }

	}

	void lex_char(){

         if( cur->ch == '?' && cur->next->next->ch=='?') {
	          STRING token = (STRING)malloc(sizeof(STRING));
              int intTokCount = 0;
	          *(token + intTokCount) = '?';
	          cur = cur->next;
	          intTokCount++;
             *(token + intTokCount) = cur->ch;
              intTokCount++;
              cur = cur->next;
              *(token + intTokCount) = '?';
              intTokCount++;
              *(token + intTokCount) = '\0';
              cur = cur->next;
              add_tok(CH,token);
              free(token);
	     }

	}

	/* lex_op()
	 * lexes operators
	 */
	 void lex_op(){

        STRING tmp_tok  = (STRING) malloc(sizeof(STRING));
        *(tmp_tok) = cur->ch;

        if(operators(tmp_tok)){

                STRING token = (STRING)malloc(sizeof(STRING));
                int intTokCount = 0;

                *(token+ intTokCount) = cur->ch;
                intTokCount++;

                int intSuccess =  0;

                char *tmp_tok2 = calloc(2,sizeof(char));
                *(tmp_tok2) = cur->next->ch;
                *(tmp_tok+1) = cur->next->ch;

                if(operators(tmp_tok2)){
                    cur = cur->next;
                    *(token + intTokCount) = cur->ch;
                    intTokCount++;
                    cur = cur->next;

                    if(operators(tmp_tok)!=2){//&&operators(chValue)&&(chValue=='='||chValue=='&')){
                        *(token+intTokCount)='\0';
                        add_tok(EROP,token);
                        free(tmp_tok);
                        free(token);
                        return;
                    }

                }
                else if(isdigit(cur->next->ch)){
                        free(tmp_tok);
                        free(token);
                        lex_int(1);
                        return;
                }
                else{
                    cur = cur->next;
                }
                free(tmp_tok);
                *(token+intTokCount)='\0';
                add_tok(OP,token);
                free(token);
        }

    }

        //char *tmp_tok = calloc(2,sizeof(char))


	void lex_punc(){
        STRING token = (STRING)malloc(sizeof(STRING));
	     *(token)= cur->ch;
	     if( pancuators(cur->ch)){
            *(token+1)= '\0';
            //cur = cur->next;
            add_tok(PUNC,token);
	     }
	     else if(!specialValue(cur->ch)&&!operators(token)){
            printf("%s",token);
            *(token+1)= '\0';
            //cur = cur->next;
            add_tok(ID,token);
	     }
	     free(token);
	}

	/*
	 * out_lex()
	 * prints the lexer output from the linked list
	 * sample line output: ('goto', 'KEYWORD')
	 */

	void out_lex(){
            /*ct = ht;
          while(ct){
            printf("%s, %s\n",ct->tok,ct->type);
            ct = ct->next;
          }*/

             initialize_symbol();
                char * prev;
                char* type;
                FILE *file;

                file = fopen(fileName,"w");
                fprintf(file,"source-file\n\n");

                for(; ht!=NULL; ht = ht->next){

                    if( ht->tok[0]== '\n' ){
                        puts(" ...Line ended.\n");
                        fprintf(file,"..Line ended.\n");

                    }//if( ht->tok[0]== '\n' ){

                    else{

                        if(lexcheck(ht->type,ID)==0) {

                            int intValue = add_symbol(ht->tok,ht->next->type,prev,type);

                            switch(intValue){

                                case 2: {
                                    printf("(%s, declaration overload error )\n", ht->tok);
                                    fprintf(file,"(%s, declaration overload error)\n", ht->tok);
                                    break;
                                }
                                case 4: {
                                    printf("(%s, undeclared error )\n", ht->tok);
                                    fprintf(file,"(%s, undeclared error)\n", ht->tok);
                                    break;
                                }
                                case 5:{
                                    printf("(%s, unrecognize error )\n", ht->tok);
                                    fprintf(file,"(%s, unrecognize error)\n", ht->tok);
                                    break;
                                }
                                default: {
                                    printf("(%s, %s)\n---------------------------\n", ht->tok, ht->type);
                                    fprintf(file,"(%s, %s)\n---------------------------\n", ht->tok, ht->type);
                                }

                            }//switch(intValue){

                        }//if(lexcheck(ht->type,ID)==0) {
                        else{
                            char strSet[100];
                            strcpy(strSet,ht->tok);
                            strSet[strlen(strSet)]= '\0';
                            printf("(%s, %s)\n\n", strSet, ht->type);
                            fprintf(file,"(%s, %s)\n\n", strSet, ht->type);
                        }

                    }//else

                prev = malloc(sizeof(ht->tok));
                type = malloc(sizeof(ht->type));
                strcpy(prev,ht->tok);
                strcpy(type,ht->type);

            }//for(; ht->next; ht = ht->next){

            free(prev);
            free(type);
            fclose(file);

    }// lex_out

	int lexcheck(char* strFirst, char* strSec){
	      int i = 0;
            //printf("lexcheck %s",strFirst);
            int intLengthInput = strlen(strFirst);//find length of test
            int intLengthLibrary = strlen(strSec);//find length of library

            if(intLengthLibrary == intLengthInput){
                for(i = 0; i<intLengthInput; i++){
                    if(strFirst[i] != strSec[i])
                        return 1;
                }
            return 0;
            }
    return 1;
    }


#endif
