
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>
#include <string.h>
#include <errno.h>
#include <conio.h>

#define FILE_NAME "sample.am"
#define SYNTABLE "syntable.txt"

#include "syn_table_def.h"
#include "open_files.h"
#include "linked_list_def.h"
#include "token_types.h"
#include "linked_list_func.h"
#include "lex.h"
#include "symbol_table.h"
#include "symbol_table_funct.h"
#include "statemachine.h"

void lex();

int main(int argc, char **args){
     // defined at "open_files.h"

     open_syntable();
     open_file();

     // defined here
     lex();
     puts(" ...Lexing ended.\n ...Press any key to exit.");
     getch();
     return 0;
}

/*
 * lex()
 * lexer mechanism
 */
void lex(){


     // defined at "linked_list_func.h"
    alloc_src();
    alloc_tok();
    char prev;
    printf("\n--------------------------------------------------------------------------------------\n\n");

    for(cur = h; cur != NULL; cur = cur->next) {

        // the following are defined at "lex.h"

        skip_comments();

        lex_idnkey();
        lex_array();
        lex_op();
        lex_int(0);
        lex_char();
        lex_str();
        lex_punc();


        //printf("skip_comments();\n");

        /*printf("lex_unrec(prev);\n");

        printf("lex_int();\n");
        lex_str();
        printf("lex_str();\n");
        */
        //lex_char();
        //printf("lex_char();\n");
        /*lex_array();
        printf("lex_array();\n");

        printf("lex_op();\n");

        printf("lex_punc();\n");
        lex_idnkey();
        printf("lex_idnkey();");
        lex_nl();*/
    }
    printf("-------------------------\n\n");
     out_lex();
     print_symbol_table();
    free_linklist();
}
