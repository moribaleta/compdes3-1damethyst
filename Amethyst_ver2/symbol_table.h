
#ifndef SYMBOL_TABLE_H

    #define SYMBOL_TABLE_H

    struct symbol_table {
        char charSymbol[20];
        int intPosition;
        char charDataType[10];
        struct symbol_table* next;
    } ;
        typedef struct symbol_table SYM_TBL;
    SYM_TBL *head, //head of list
    *curr,                 //current
    *tmp_sym; //temp


#endif // SYMBOL_TABLE_H
