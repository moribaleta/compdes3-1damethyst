/*
 * linked_list_def.h
 * defines linked list types
 */
#ifndef LINKED_LIST_DEF_H_
	#define LINKED_LIST_DEF_H_

 	/* source file's linked list
 	 * storage for the content of source file to be read
 	 * node values are in char
 	 *
 	 * example node content:
 	 * '#'
 	 * another example node:
	 * 'i'
	 *
	 * list excerpt (combination of nodes):
	 * '#' 'i' 'n' 'c' 'l' 'u' 'd' 'e' '\n'
 	 */
	typedef struct src_node SRC_LST;
	struct src_node {
    	char ch;
    	SRC_LST *next;
	};

	/* token's linked list
	 * storage for valid tokens and its corresponding type
	 * node values are in string
	 *
	 * example node content:
	 * tok = "goto"
	 * type = "IDENTIFIER"
	 *
	 * another example node:
	 * tok = "28"
	 * type = "INTEGER"
	 *
	 * list excerpt (combination of nodes):
	 * ("goto", "IDENTIFIER") ("28", "INTEGER")
	 */
	typedef struct tok_node TOK_LST;
	struct tok_node {
	    char *tok;
	    char *type;
	    TOK_LST *next;
	};

	// head node of token's list
	TOK_LST *ht,

	// current node or counter node of token's list
	*ct,

	// temporary node for every new node created
	// will be used in adding nodes
	*tmp;

	// head of source's list
	SRC_LST *h,

	// current node or counter node of source's list
	*cur;

#endif

