/*
 * syn_table_def.h
 * defines syntax tables
 */
#ifndef SYN_TABLE_DEF_H_
	#define SYN_TABLE_DEF_H_

	// STRING data type
	typedef char* STRING;

	// syntax tables
	STRING syn_ops, // same-char operator syntax table
	syn_opd, 		// different-char operator syntax table
	syn_punc, 		// punctuator syntax table
	syn_key, 		// keyword operator syntax table
	syn_coms, 		// single-line comment operator syntax table
	syn_comm,		// multi-line comment operator syntax table
	syn_noise, //noise words
	syn_datatypes; //data types

#endif

