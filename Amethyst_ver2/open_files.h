#ifndef OPEN_FILES_H_
	#define OPEN_FILES_H_

	// file pointer to source file
	FILE *fp;

	void open_file();
	void open_syntable();
    char fileName[50];
    char fileSymbolFile[50];

	void open_file(){
	    char chString[100];
	    printf("enter the filename to be executed: ");
	    scanf("%s",&chString);
	    //strcpy(fileName,chString);
	    strncpy(fileName,chString,(strlen(chString)-3));
        strcpy(fileSymbolFile,fileName);
        strcat(fileSymbolFile,"_symbol_table.txt");
	     strcat(fileName,"-output.txt");

	     fp = fopen(chString, "r");
	     if( fp == NULL ) {
	          perror("file error");
	          exit(1);
	      }
	}

	void open_syntable(){
	     FILE *fs = fopen(SYNTABLE, "r");

	     syn_ops = calloc(100, sizeof (char));
	     syn_opd = calloc(100, sizeof (char));
	     syn_punc = calloc(100, sizeof (char));
	     syn_key = calloc(100, sizeof (char));
	     syn_coms = calloc(100, sizeof (char));
	     syn_comm = calloc(100, sizeof (char));
         syn_noise = calloc(100,sizeof(char));
         syn_datatypes= calloc(100,sizeof(char));

	     fgets(syn_ops, 100, fs);
	     fgets(syn_opd, 100, fs);
	     fgets(syn_punc, 100, fs);
	     fgets(syn_key, 100, fs);
	     fgets(syn_coms, 100, fs);
	     fgets(syn_comm, 100, fs);
	     fgets(syn_noise,100,fs);
	     fgets(syn_datatypes,100,fs);
	     fclose(fs);
	}

#endif
