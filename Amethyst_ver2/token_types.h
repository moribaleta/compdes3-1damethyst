/*
 * token_types.h
 * defines token types and their names
 */
#ifndef TOKEN_TYPES_H_
	#define TOKEN_TYPES_H_

	#define OP "OPERATOR"
	#define KEY "KEYWORD"

	#define ID "IDENTIFIER"
	#define PUNC "PUNCTUATOR"
	#define NUM "NUMERIC VALUE"
	#define STR "STRING"
	#define INT "INTEGER"
	#define FLT "FLOAT"
	#define NL "NEWLINE"
    #define NW "NOISEWORDS"
    #define DT "DATATYPES"
    #define CH "CHARACTER"
    #define COM "COMMENTS"
    #define BL "BOOLEAN"
    #define NU "NULL"
    #define EROP "ERROR UNDEFINED OPERATOR "
    #define ERUV "ERROR UNRECOGNIZE VALUE"
    #define ARR "ARRAY"

#endif

