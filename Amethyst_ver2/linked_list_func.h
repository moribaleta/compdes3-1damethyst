
#ifndef LINKED_LIST_FUNC_H_
	#define LINKED_LIST_FUNC_H_

	void alloc_src();
	void alloc_tok();
	void add_tok(STRING type,STRING *token);

	FILE *fp;

	/*
	 * alloc_src()
	 * Initializes src linked list, then
	 * puts the source file's text to the src linked list, defined by LIST h
	 *
	 */
	void alloc_src(){
	     // buf list
	     // head
	     h = malloc(sizeof (SRC_LST));
	     h->ch = ' ';
	     h->next = NULL;

	     // current
	     cur = h;

	     // put src to list
	     char c = EOF;
	     while( (c = fgetc(fp)) != EOF ) {
	          SRC_LST *temp = malloc(sizeof (SRC_LST));
	          temp->ch = c;
	          temp->next = NULL;
	          cur->next = temp;
	          cur = cur->next;
	     }
        SRC_LST *temp = malloc(sizeof (SRC_LST));
        temp->ch = ' ';
        temp->next = NULL;
        cur->next = temp;
        cur = cur->next;
	}
	/*
	 * alloc_tok()
	 * Initializes token's linked list
	 *
	 */
	void alloc_tok(){

	     ht = malloc(sizeof (TOK_LST));
	     ht->tok = calloc(1, sizeof (char));
	     ht->type = calloc(1, sizeof (char));
	     strcpy(ht->tok, "");
	     strcpy(ht->type, "");
	     ht->next = NULL;

	     ct = ht;
	}
	/*
	 * add_tok()
	 * adds tuple (token, token type) to token list
	 * @param type: token type
	 *
	 */
	void add_tok(STRING type, STRING *token){
        if(lexcheck(type,STR)==0){
            token[strlen(token)] = '\0';
            printf("string token: %s\n",token);
        }
	     tmp = malloc(sizeof (TOK_LST));
	     tmp->tok = malloc(sizeof token);
	     tmp->type = malloc(sizeof type);
	     strcpy(tmp->tok, token);
	     tmp->tok[strlen(tmp->tok)]='\0';
	     strcpy(tmp->type, type);
	     //printf("token added :%s,%s\n",tmp->tok,tmp->type);
	     tmp->next = NULL;

	     ct->next = tmp;
	     ct = ct->next;
	}

	void free_linklist(){
        free(tmp);
        free(cur);
        free(ht);
        free(ct);
        free(h);
	}

#endif
