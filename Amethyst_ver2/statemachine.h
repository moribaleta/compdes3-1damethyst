#ifndef STATEMACHINE_H

    #define STATEMACHINE_H
/* commented 1st version of state machine

	int keywords(char *chString){
		char chValue[30];
		strcpy(chValue,chString);

		//return 0 if not found
        //return 1 if found

		switch(chValue[0]){
			case 'a':{
				if(chValue[1]=='i'){
					if(chValue[2]=='n'){
						if(chValue[3]!=NULL){
							return 0; // exceed max length ain
						}
						else{
							return 1; //found
						}
					}
				}
				else if(chValue[1]=='o'){
					if(chValue[2]=='u'){
						if(chValue[3]=='t'){
							if(chValue[4]!=NULL){
								return 0; // exceed max length of aout
							}
							else{
								return 1; //found
							}
						}
					}
				}
				return 0;
			}// =============== end of case ain or aout =============
			case 'i':{
				if(chValue[1]=='f'){
					if(chValue[2]!=NULL){
						return 0; // exceed max length of if
					}
					else{
						return 1; //found
					}
				}
				return 0;
			}// =============== end of case if =============
			case 'e':{
				if(chValue[1]=='l'){
					if(chValue[2]=='s'){
						if(chValue[3]=='e'){
							if(chValue[4]!=NULL){
								return 0; // exceed max length else
							}
							else{
								return 1; //found
							}
						}
					}
				}
				return 0;
			}// =============== end of case else =============
			case 'f':{
				if(chValue[1]=='o'){
					if(chValue[2]=='r'){
						if(chValue[3]!=NULL){
							return 0; // exceed max length for
						}
						else{
							return 1; //found
						}
					}
				}
				return 0;
			}// =============== end of case for =============
			case 'w':{
				if(chValue[1]=='h'){
					if(chValue[2]=='i'){
						if(chValue[3]=='l'){
							if(chValue[4]=='e'){
								if(chValue[5]!=NULL){
									return 0; // exceed max length of while
								}
								else{
									return 1; // found
								}
							}
						}
					}
				}
				return 0;
			}// =============== end of case while =============
			case 'r':{
				if(chValue[1]=='e'){
					if(chValue[2]=='t'){
						if(chValue[3]=='u'){
							if(chValue[4]=='r'){
								if(chValue[5]=='n'){
									if(chValue[6]!=NULL){
										return 0; // exceed max length of return
									}
									else{
										return 1; // found
									}
								}
							}
						}
					}
				}
				return 0;
			}// =============== end of case return =============
			case 'd':{
				if(chValue[1]=='o'){
					if(chValue[2]!=NULL){
						return 0; // exceed max length of do
					}
					else{
						return 1; // found
					}
				}
				return 0;
			}// =============== end of case do =============
			case 's':{
				if(chValue[1]=='w'){
					if(chValue[2]=='i'){
						if(chValue[3]=='t'){
							if(chValue[4]=='c'){
								if(chValue[5]=='h'){
									if(chValue[6]!=NULL){
										return 0; // exceed max length of switch
									}
									else{
										return 1; // found
									}
								}
							}
						}
					}
				}
				return 0;
			}// =============== end of case switch =============
			case 'c':{
				if(chValue[1]=='a'){
					if(chValue[2]=='s'){
						if(chValue[3]=='e'){
							if(chValue[4]!=NULL){
								return 0; // exceed max length of case
							}
							else{
								return 1; // found
							}
						}
					}
				}
				return 0;
			}// =============== end of case case =============
			case 'v':{
				if(chValue[1]=='o'){
					if(chValue[2]=='i'){
						if(chValue[3]=='d'){
							if(chValue[4]!=NULL){
								return 0; // exceed max length of void
							}
							else{
								return 1; // found
							}
						}
					}
				}
				return 0;
			}// =============== end of case void =============
		}
		return 0;
	}// ================ end of keywords method =================

	int datatypes(char *chString){
        char chValue[30];
        strcpy(chValue,chString);

        //return 0 if not found
        //return 1 if found

        switch(chValue[0]){
			case 'i':{
				if(chValue[1]=='n'){
					if(chValue[2]=='t'){
						if(chValue[3]!=NULL){
							return 0; // exceed max length of int
						}
						else{
							return 1; // found
						}
					}
				}
				return 0;
			}// =============== end of case int =============
			case 'c':{
				if(chValue[1]=='h'){
					if(chValue[2]=='a'){
						if(chValue[3]=='r'){
							if(chValue[4]!=NULL){
								return 0; // exceed max length of char
							}
							else{
								return 1; // found
							}
						}
					}
				}
				return 0;
			}// =============== end of case char =============
			case 'f':{
				if(chValue[1]=='l'){
					if(chValue[2]=='o'){
						if(chValue[3]=='a'){
							if(chValue[4]=='t'){
								if(chValue[5]!=NULL){
									return 0; // exceed max length of float
								}
								else{
									return 1; // found
								}
							}
						}
					}
				}
				return 0;
			}// =============== end of case float =============
			case 'd':{
				if(chValue[1]=='o'){
					if(chValue[2]=='u'){
						if(chValue[3]=='b'){
							if(chValue[4]=='l'){
								if(chValue[5]=='e'){
									if(chValue[6]!=NULL){
										return 0; // exceed max length of double
									}
									else{
										return 1; // found
									}
								}
							}
						}
					}
				}
				return 0;
			}// =============== end of case double =============
			case 'b':{
				if(chValue[1]=='o'){
					if(chValue[2]=='o'){
						if(chValue[3]=='l'){
									if(chValue[4]!=NULL){
										return 0; // exceed max length of double
									}
									else{
										return 1; // found
									}
								}
                        }
                    }
				return 0;
			}// =============== end of case double =============
		}
		return 0;

	}// ================ end of datatypes method =================

    int noisewords(char *chString){
        char chValue[30];
        strcpy(chValue,chString);

        //return 0 if not found
        //return 1 if found

        switch(chValue[0]){
            case 'g':{
                if(chValue[1]=='o'){
                        if(chValue[2]=='t'){
                            if(chValue[3]=='o'){
                                if(chValue[4]!=NULL){
                                    return 0; // exceed max length of goto
                                }
                                else{
                                    return 1; // found
                                }
                            }
                        }
                }
                return 0;
            }// =============== end of case goto =============
            case 'h':{
                if(chValue[1]=='e'){
                        if(chValue[2]=='r'){
                            if(chValue[3]=='e'){
                                if(chValue[4]!=NULL){
                                    return 0; // exceed max length of here
                                }
                                else{
                                    return 1; // found
                                }
                            }
                        }
                }
                return 0;
            }// =============== end of case here =============

            case 'c':{
                if(chValue[1]=='o'){
                        if(chValue[2]=='n'){
                            if(chValue[3]=='t'){
                                if(chValue[4]=='i'){
                                    if(chValue[5]=='n'){
                                        if(chValue[6]=='u'){
                                            if(chValue[7]=='e'){
                                                if(chValue[8]!=NULL){
                                                    return 0; // exceed max length of continue
                                                }
                                                else{
                                                    return 1; // found
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                return 0;
            }// =============== end of case continue =============

            case 'b':{
                if(chValue[1]=='r'){
                    if(chValue[2]=='e'){
                        if(chValue[3]=='a'){
                            if(chValue[4]=='k'){
                                if(chValue[5]!=NULL){
                                    return 0; // exceed max length of break
                                }
                                else{
                                    return 1; // found
                                }
                            }
                        }
                    }
                }
				return 0;
            }//// =============== end of case break =============
        }
        return 0;

    }// =============== end of noisewords method =============
*/


	int keywords(char *chString){
		char chValue[30];
		strcpy(chValue,chString);

		//return 0 if not found
        //return 1 if found

		switch(chValue[0]){
			case 'a':{
				if(chValue[1]=='i'){
					if(chValue[2]=='n'){
						if(chValue[3]!=NULL){
							return 0; // exceed max length ain
						}
						else{
							return 1; //found
						}
					}
				}
				else if(chValue[1]=='o'){
					if(chValue[2]=='u'){
						if(chValue[3]=='t'){
							if(chValue[4]!=NULL){
								return 0; // exceed max length of aout
							}
							else{
								return 1; //found
							}
						}
					}
				}
				return 0;
			}// =============== end of case ain or aout =============
			case 'i':{
				if(chValue[1]=='f'){
					if(chValue[2]!=NULL){
						return 0; // exceed max length of if
					}
					else{
						return 1; //found
					}
				}
				return 0;
			}// =============== end of case if =============
			case 'e':{
				if(chValue[1]=='l'){
					if(chValue[2]=='s'){
						if(chValue[3]=='e'){
							if(chValue[4]!=NULL){
								return 0; // exceed max length else
							}
							else{
								return 1; //found
							}
						}
						else if(chValue[3]=='i'){
                            if(chValue[4]=='f'){
                                 if(chValue[5]!=NULL){
                                    return 0;
                                 }
                                 else{
                                    return 1;
                                 }
                            }
						}
					}
				}
				return 0;
			}// =============== end of case else =============
			case 'f':{
				if(chValue[1]=='o'){
					if(chValue[2]=='r'){
						if(chValue[3]!=NULL){
							return 0; // exceed max length for
						}
						else{
							return 1; //found
						}
					}
				}
				return 0;
			}// =============== end of case for =============
			case 'r':{
				if(chValue[1]=='e'){
					if(chValue[2]=='t'){
						if(chValue[3]=='u'){
							if(chValue[4]=='r'){
								if(chValue[5]=='n'){
									if(chValue[6]!=NULL){
										return 0; // exceed max length of return
									}
									else{
										return 1; // found
									}
								}
							}
						}
					}
				}
				return 0;
			}// =============== end of case return =============
			case 'd':{
				if(chValue[1]=='e'){
					if(chValue[2]=='f'){
						if(chValue[3]=='a'){
							if(chValue[4]=='u'){
								if(chValue[5]=='l'){
									if(chValue[6]=='t'){
										if(chValue[7]!=NULL){
											return 0; // exceed max length of default
										}
										else{
											return 1; // found
										}
									}
								}
							}
						}
					}
				}
				return 0;
			}// =============== end of case default =============
		}
		return 0;

	}// ================ end of keywords method =================

	int datatypes(char *chString){
        char chValue[30];
        strcpy(chValue,chString);

        //return 0 if not found
        //return 1 if found

        switch(chValue[0]){
			case 'i':{
				if(chValue[1]=='n'){
					if(chValue[2]=='t'){
						if(chValue[3]!=NULL){
							return 0; // exceed max length of int
						}
						else{
							return 1; // found
						}
					}
				}
				return 0;
			}// =============== end of case int =============
			case 's':{
				if(chValue[1]=='t'){
					if(chValue[2]=='r'){
						if(chValue[3]=='i'){
							if(chValue[4]=='n'){
								if(chValue[5]=='g'){
									if(chValue[6]!=NULL){
										return 0; // exceed max length of string
									}
									else{
										return 1; // found
									}
								}
							}
						}
					}
				}
			}// =============== end of case string =============
			case 'f':{
				if(chValue[1]=='l'){
					if(chValue[2]=='o'){
						if(chValue[3]=='a'){
							if(chValue[4]=='t'){
								if(chValue[5]!=NULL){
									return 0; // exceed max length of float
								}
								else{
									return 1; // found
								}
							}
						}
					}
				}
				return 0;
			}// =============== end of case float =============
			case 'c':{
				if(chValue[1]=='h'){
					if(chValue[2]=='a'){
						if(chValue[3]=='r'){
							if(chValue[4]!=NULL){
								return 0; // exceed max length of char
							}
							else{
								return 1; // found
							}
						}
					}
				}
				return 0;
			}// =============== end of case char =============
			case 'b':{
				if(chValue[1]=='o'){
					if(chValue[2]=='o'){
						if(chValue[3]=='l'){
							if(chValue[4]!=NULL){
								return 0; // exceed max length of bool
							}
							else{
								return 1; // found
							}
						}
					}
				}
			}// =============== end of case bool =============
		}
		return 0;

	}// ================ end of datatypes method =================

    int noisewords(char *chString){
        char chValue[30];
        strcpy(chValue,chString);

        //return 0 if not found
        //return 1 if found

        switch(chValue[0]){
            case 'g':{
                if(chValue[1]=='o'){
                        if(chValue[2]=='t'){
                            if(chValue[3]=='o'){
                                if(chValue[4]!=NULL){
                                    return 0; // exceed max length of goto
                                }
                                else{
                                    return 1; // found
                                }
                            }
                        }
                }
                return 0;
            }// =============== end of case goto =============
            case 'h':{
                if(chValue[1]=='e'){
                        if(chValue[2]=='r'){
                            if(chValue[3]=='e'){
                                if(chValue[4]!=NULL){
                                    return 0; // exceed max length of here
                                }
                                else{
                                    return 1; // found
                                }
                            }
                        }
                }
                return 0;
            }// =============== end of case here =============
            case 'b':{
                if(chValue[1]=='r'){
                    if(chValue[2]=='e'){
                        if(chValue[3]=='a'){
                            if(chValue[4]=='k'){
                                if(chValue[5]!=NULL){
                                    return 0; // exceed max length of break
                                }
                                else{
                                    return 1; // found
                                }
                            }
                        }
                    }
                }
				return 0;
            }//// =============== end of case break =============
        }
        return 0;

    }// =============== end of noisewords method =============

	int constantvalues(char *chString){
		char chValue[30];
		strcpy(chValue,chString);

		//return 0 if not found
		//return 1 if found

		switch(chValue[0]){
			case 't':{
				if(chValue[1]=='r'){
					if(chValue[2]=='u'){
						if(chValue[3]=='e'){
							if(chValue[4]!=NULL){
								return 0; // exceed max length of true
							}
							else{
								return 1; // found
							}
						}
					}
				}
			}//// =============== end of case true =============
			case 'f':{
				if(chValue[1]=='a'){
					if(chValue[2]=='l'){
						if(chValue[3]=='s'){
							if(chValue[4]=='e'){
								if(chValue[5]!=NULL){
									return 0; // exceed max length of false
								}
								else{
									return 1; // found
								}
							}
						}
					}
				}
			}//// =============== end of case false =============
			case 'n':{
				if(chValue[1]=='i'){
					if(chValue[2]=='l'){
						if(chValue[3]!=NULL){
							return 0; // exceed max length of nil
						}
						else{
							return 2; // found
						}
					}
				}
			}//// =============== end of case nil =============
		}
		return 0;

	}// =============== end of constantvalues method =============

    int pancuators(char *chValue){
        if(chValue=='{'||chValue=='}'||chValue=='('||chValue==')'||chValue==';'||chValue==','||chValue==':'||chValue=='['||chValue==']'||chValue=='?')
            return 1;
        return 0;
    }

    int specialValue(char *chValue){
        if(chValue=='\n'||chValue=='\t'||chValue==' ')
            return 1;
        return 0;
    }

	char* operators(char *chString){
        //printf("lex_op: %s\n\n", chString);
		char chValue[30];
		strcpy(chValue,chString);
		char *str = malloc(40 * sizeof(char));
		

		//return NULL if not found
        //return  if found

		switch(chValue[0]){

			case '+':{
			    if(chValue[1]=='+'){
					if(chValue[2]!=NULL){
						return NULL; // exceed max length of ++
					}
					else{
						strcpy(str, "INCREMENT OPERATOR");
						return str;//found
					}
				}
				if(chValue[1]!=NULL){
					return NULL; // exceed max length of +
				}
				else{
					strcpy(str, "ADDITION OPERATOR");
					return str;//found
				}
			}//// =============== end of case + =============

			case '-':{
			    if(chValue[1]=='-'){
					if(chValue[2]!=NULL){
						return NULL; // exceed max length of --
					}
					else{
						strcpy(str, "DECREMENT OPERATOR");
						return str;//found
					}
				}
				if(chValue[1]!=NULL){
					return NULL;// exceed max length of -
				}
				else{
					strcpy(str, "SUBTRACTION OPERATOR");
					return str;//found
				}
			}//// =============== end of case - =============

			case '=':{
				if(chValue[1]=='='){
					if(chValue[2]!=NULL){
						return NULL; // exceed max length of ==
					}
					else{
						strcpy(str, "EQUAL TO OPERATOR");
						return str;//found
					}
				}
				else if(chValue[1]!=NULL){
					return NULL;	// exceed max length of =
				}
				else{
					strcpy(str, "EQUAL TO OPERATOR");
					return str;//found
				}
			}//// =============== end of case = or == =============

			case '&':{
				if(chValue[1]=='&'){
					if(chValue[2]!=NULL){
						return NULL;// exceed max length of &&
					}
					else{
						strcpy(str, "AND OPERATOR");
						return str;//found
					}
				}
			}//// =============== end of case && =============

			case '|':{
				if(chValue[1]=='|'){
					if(chValue[2]!=NULL){
						return NULL;// exceed max length of ||
					}
					else{
						strcpy(str, "OR OPERATOR");
						return str;//found
					}
				}
			}//// =============== end of case || =============

			case '*':{
				if(chValue[1]!=NULL){
					return NULL;// exceed max length of *
				}
				else{
					strcpy(str, "MULTIPLICATION OPERATOR");
					return str;//found
				}
			}//// =============== end of case * =============

			case '$':{
				if(chValue[1]!=NULL){
					return NULL;// exceed max length of $
				}
				else{
					strcpy(str, "DIVISION OPERATOR");
					return str;//found
				}
			}//// =============== end of case $ =============

			case '%':{
				if(chValue[1]!=NULL){
					return NULL; // exceed max length of %
				}
				else{
					strcpy(str, "MODULUS OPERATOR");
					return str;//found
				}
			}//// =============== end of case % =============

			case '^':{
				if(chValue[1]!=NULL){
					return NULL; //exceed max length of ^
				}
				else{
					strcpy(str, "POWER OPERATOR");
					return str;//found
				}
			}//// =============== end of case ^ =============

			case '~':{
				if(chValue[1]!=NULL){
					return NULL; // exceed max length of ~
				}
				else{
					strcpy(str, "DIV OPERATOR");
					return str;//found
				}
			}//// =============== end of case ~ =============

			case '>':{
				if(chValue[1]=='='){
					if(chValue[2]!=NULL){
						return NULL; // exceed max length of >=
					}
					else{
						strcpy(str, "GREATER THAN OR EQUAL TO OPERATOR");
						return str;//found
					}
				}
				else if(chValue[1]!=NULL){
						return NULL; // exceed max length of >
				}
				else{
					strcpy(str, "GREATER THAN OPERATOR");
					return str;//found
				}
			}//// =============== end of case > or >= =============
			case '<':{
				if(chValue[1]=='='){
					if(chValue[2]!=NULL){
						return NULL; // exceed max length of <=
					}
					else{
						strcpy(str, "LESS THAN OR EQUAL TO OPERATOR");
						return str;//found
					}
				}
				if(chValue[1]!=NULL){
					return NULL; //exceed max length of <
				}
				else{
					strcpy(str, "LESS THAN OPERATOR");
					return str;//found
				}
			}//// =============== end of case < or <= =============
			case '!':{
				if(chValue[1]=='='){
					if(chValue[2]!=NULL){
						return NULL; // exceed max length of !=
					}
					else{
						strcpy(str, "NOT EQUAL TO OPERATOR");
						return str;//found
					}
				}
				if(chValue[1]!=NULL){
					return NULL; // exceed max length of !
				}
				else{
					strcpy(str, "NOT OPERATOR");
					return str;//found
				}
			}//// =============== end of case ! or != =============
		}
		return NULL;

	}// =============== end of operators method =============


#endif // STATEMACHINE_H
