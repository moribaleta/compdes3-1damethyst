#ifndef SYMBOL_TABLE_FUNCT_H

    #define SYMBOL_TABLE_FUNCT_H

    void initialize_symbol();
	int add_symbol(char *symbol_table,char*next_type, char *prev, char *type);
	void print_symbol_table();
	void saveIdentifier(char*symbol, char*type);
	/*
	 * alloc_tok()
	 * Initializes token's linked list
	 *
	 */
	 int count = 1;

    void initialize_symbol(){
        head = NULL;
    }

    /*
    interface for connecting
    the lex_out identifier operation
    to symbol_table
    */
	int add_symbol(char* symbol,char*next_type,char *prev, char *type){
        tmp_sym  = head;

        while(tmp_sym){
            if(lexcheck(tmp_sym->charSymbol,symbol)==0){
                if(lexcheck(type,"DATATYPES")==0){
                    return 2;
                }
                return 1;
            }
            tmp_sym = tmp_sym->next;
        }

        //printf("add identifier: %s \n",symbol);
        if(lexcheck(type,DT)==0&&isalpha(symbol[0])){
             saveIdentifier(symbol,prev);
            return 0;
        }else{
            if(lexcheck(next_type,OP)==0)
                return 4;
            else{
                  saveIdentifier(symbol,"unrecognize");
                 return 5;
            }
        }
         /*ct->next = tmp;
	     ct = ct->next;*/
	}

    /*
        method used
        for saving data
        to the symbol table
    */
	void saveIdentifier(char *symbol, char *type){
                curr = (SYM_TBL *)malloc(sizeof(SYM_TBL));
                strcpy( curr->charSymbol , symbol);
                curr->intPosition = count++;
                strcpy(curr->charDataType, type);
                //printf("saved %s %d \n",curr->charSymbol, curr->intPosition);
                curr->next = head;
                head = curr;
    }

    /*
    outputs the node that is stored
    in the symbol table
    */
	void print_symbol_table(){
        curr = head;
        FILE *file;
        file  = fopen(fileSymbolFile,"w");
        fprintf(file,"\n\n----------------\nsymbol table\n");

        while(curr){
            fprintf(file,"%s , %s, %d\n", curr->charSymbol,curr->charDataType,curr->intPosition);
            printf("%s , %s, %d\n", curr->charSymbol,curr->charDataType,curr->intPosition);
            curr = curr->next;
        }

        fclose(file);
        free(head);
        free(curr);
        free(tmp_sym);
	}


#endif
